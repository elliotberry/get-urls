const isURL = require('is-url');

module.exports = function(args) {
    return new Promise(function(res,rej) {
        if (args.length > 0) {
            let argStr = args.join(" ");
            if (argStr.indexOf("--h") > -1 || argStr.indexOf("--help") > -1 || argStr.indexOf("-h") > -1) {
                console.log("lily v. 0.0.1; Example use: lly <folder1> <folder2>");
            } else {
                if (isURL(args[2])) {
                res(args[2]);
                } else {
                    console.log("Incorrect arg. Need a URL.");
                }
            }
        } else {
            console.log("Not enough arguments.");
        }
    });
}