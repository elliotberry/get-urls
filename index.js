const gru = require('./lib/get-site-urls');
const fs = require('fs');
const clip = require('./lib/cli-parse');
//Global.logger = require('./lib/logger');


async function main(url) {
    const links = await gru(url);
    fs.writeFileSync('./out.json', JSON.stringify(links));
    Global.logger.info("got 'em! saved to ./out.json");
}

clip(process.argv).then( function(url) {
    //Global.logger.info("got 'em! saved to ./out.json");
    main(url);
});